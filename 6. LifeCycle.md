-   When container starts – a Spring bean needs to be instantiated, based on Java or XML bean definition.
-    It may also be required to perform some post-initialization steps to get it into a usable state.
-   After that, when the bean is no longer required, it will be removed from the IoC container.
-   **Spring bean factory** is responsible for managing the life cycle of beans created through spring container.

    ![](https://www.websparrow.org/wp-content/uploads/2018/07/spring-bean-life-cycle-management-example.png)
------------------
##  Life cycle callbacks
-   Spring bean factory controls the creation and destruction of beans. 
-   To execute some custom code, it provides the call back methods which can be categorized broadly in two groups
    -   Post-initialization call back methods
    -   Pre-destruction call back methods
#  Methods
##   InitializingBean and DisposableBean
-   The InitializingBean interface allows a bean to perform initialization work after all necessary properties on the bean have been set by the container.
-   The **InitializingBean** interface specifies a single method

    ```void afterPropertiesSet() throws Exception```
-   This is **not a preferrable** way to initialize the bean because it tightly couple your bean class with spring container. A better approach is to use “init-method” attribute in bean definition in xml file.
-   Similarly, implementing the DisposableBean interface allows a bean to get a callback when the container containing it is destroyed.
-   The **DisposableBean** interface specifies a single method

    ```public void destroy() throws Exception ```

##  Custom init() and destroy() methods
The default init and destroy methods in bean configuration file can be defined in two ways
-   Bean local definition applicable to a single bean
    ```xml
    <bean id="demoBean" class="com.howtodoinjava.task.DemoBean"
        init-method="customInit"
        destroy-method="customDestroy"></bean>
    ```    
-   Global definition applicable to all beans defined in beans context
    ```xml
    <beans default-init-method="customInit" default-destroy-method="customDestroy">
    ```
##  @PostConstruct and @PreDestroy
You can use annotations also for specifying life cycle methods
-   @PostConstruct annotated method will be invoked after the bean has been constructed using default constructor and just before it’s instance is returned to requesting object.
-   @PreDestroy annotated method is called just before the bean is about be destroyed inside bean container.