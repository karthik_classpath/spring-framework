-   A bean post processor allows for c**ustom modification of new bean instances** created by spring bean factory. 
-    If you want to implement some custom logic after the Spring container finishes instantiating, configuring, and initializing a bean, we can plug in one or more BeanPostProcessor implementations.
-   BeanPostProcessor interface consists of exactly two callback methods i.e. 
-   The BeanPostProcessors operate on bean (or object) instances, which means that the Spring IoC container instantiates a bean instance and then BeanPostProcessor interfaces do their work.
-   An ApplicationContext automatically detects any beans that are defined with the implementation of the BeanPostProcessor interface and registers these beans as postprocessors, to be then called appropriately by the container upon bean creation.
**postProcessBeforeInitialization()** and **postProcessAfterInitialization()**.
##  How to create BeanPostProcessor
To create a bean post processor
-   implement the BeanPostProcessor interface.
-   implement the callback methods.
-   Configure it in xml.
-----------------

#   BeandFactoryPostProcessor
-   Factory hook that allows for custom modification of an application context's bean definitions, adapting the bean property values of the context's underlying bean factory.
-   Useful for custom config files targeted at system administrators that override bean properties configured in the application context.
-   An ApplicationContext **auto-detects** BeanFactoryPostProcessor beans in its bean definitions and applies them before any other beans get created. 