package io.classpath.demo;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class Triangle implements	Shape{
	
	private	Point	pointA;
	private	Point	pointB;
	private	Point	pointC;
	
	public Point getPointA() {
		return pointA;
	}
	public void setPointA(Point pointA) {
		this.pointA = pointA;
	}
	public Point getPointB() {
		return pointB;
	}
	public void setPointB(Point pointB) {
		this.pointB = pointB;
	}
	public Point getPointC() {
		return pointC;
	}
	public void setPointC(Point pointC) {
		this.pointC = pointC;
	}

	public	void	draw() {
		System.out.println(pointA);
		System.out.println(pointB);
		System.out.println(pointC);
	}
	
	/*
	 * @Override public void afterPropertiesSet() throws Exception {
	 * System.out.println("Inside InitializingBean method"); }
	 * 
	 * @Override public void destroy() throws Exception {
	 * System.out.println("Inside DisposableBean method"); }
	 */
	public	void	myInit() {
		System.out.println("My custom initial method");
	}
	public void myClean() {
		System.out.println("My custom cleaning method");
	}
}
