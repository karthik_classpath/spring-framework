package io.classpath.demo;

import org.springframework.beans.factory.config.BeanPostProcessor;

public class DisplayNameBeanPostProcessor implements	BeanPostProcessor{
	@Override
	public	Object	postProcessBeforeInitialization(Object	bean,String	beanName) {
		System.out.println("In before initialization method, bean name ->"+beanName);
		return	bean;
	}
	@Override
	public	Object	postProcessAfterInitialization(Object	bean,String	beanName) {
		System.out.println("In after initialization method, bean name ->"+beanName);
		return	bean;
	}
}
