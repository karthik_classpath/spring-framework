package io.classpath.demo;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
//import org.springframework.beans.factory.annotation.Required;

@Service
public class Circle implements	Shape, ApplicationEventPublisherAware{
	private	Point	center;
	private	MessageSource	messageSource;
	private ApplicationEventPublisher	publisher;
	
	public MessageSource getMessageSource() {
		return messageSource;
	}
	@Autowired
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public Point getCenter() {
		return center;
	}
	
	@Resource
	public void setCenter(Point center) {
		this.center = center;
	}

	@Override
	public void draw() {
		System.out.println(this.messageSource.getMessage("drawing.circle", null, "Default drawing circle msg", null));
		System.out.println(this.messageSource.getMessage("drawing.center", new Object[] {center.getX(), center.getY()}, "Default drawing circle msg", null));
	
		DrawEvent	drawEvent	=	new	DrawEvent(this);
		publisher.publishEvent(drawEvent);
	}
	
	@PostConstruct
	public void initCircle() {
		System.out.println("Initialization in circle");
	}
	@PreDestroy
	public void destroyCircle() {
		System.out.println("Destroy in circle");
	}
	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
			publisher	=	applicationEventPublisher;
	}
	
}
